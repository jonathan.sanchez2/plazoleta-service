package com.pragma.powerup.factory;

import com.pragma.powerup.domain.model.RestaurantModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FactoryRestaurantDataTest {

    public static RestaurantModel getRestaurantModelWithIdNull(){

        RestaurantModel restaurantModel = new RestaurantModel(
                null,"Otto Grill","4434",
                "Av Aviacion 432","989898977",
                "carn.jpg",2L
        );

        return restaurantModel;
    }

    public static RestaurantModel getRestaurantModel(){

        return new RestaurantModel(
                1L,"Otto Grill","4434",
                "Av Aviacion 432","989898977",
                "carn.jpg",2L
        );

    }

    public static Optional<RestaurantModel> getOptionalRestaurantModel(){

        return Optional.of(getRestaurantModel());

    }

    public static List<RestaurantModel> getListRestaurantModel(){

        List<RestaurantModel> restaurantModel = new ArrayList<>();
        restaurantModel.add(getRestaurantModel());

        return restaurantModel;
    }

    public static Long getExistingIdRestaurant(){
        return 1L;
    }

    public static Long getNoExistingIdRestaurant(){
        return 10L;
    }
}
