package com.pragma.powerup.factory;

import com.pragma.powerup.domain.model.CategoryModel;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.RestaurantModel;

import java.util.Optional;

public class FactoryDishDataTest {

    public static DishModel getDishModelWithIdNull(){

        CategoryModel category = new CategoryModel(1L,null,null);
        RestaurantModel restaurant = new RestaurantModel(1L,null,null,null,null,null,null);

        return new DishModel(
                null,"Papa rellena","papa rellena de carne y cebolla",
                6.99,"https://paparellena.com.pe", false, category,
                restaurant
        );

    }

    public static DishModel getDishModelOnlyWithPriceAndDescription(){

        return new DishModel(
                null,null,"papa rellena de carne y cebolla editado",
                8.99,null, false, null, null
        );

    }

    public static DishModel getDishModel(){

        CategoryModel category = new CategoryModel(1L,null,null);
        RestaurantModel restaurant = new RestaurantModel(1L,null,null,null,null,null,null);

        return new DishModel(
                1L,"Papa rellena","papa rellena de carne y cebolla",
                6.99,"https://paparellena.com.pe", false, category,
                restaurant
        );

    }

    public static Optional<DishModel> getOptionalDishModel(){

        return Optional.of(getDishModel());

    }


    public static Long getExistingIdDish(){
        return 1L;
    }

    public static Long getNoExistingIdDish(){
        return 10L;
    }
}
