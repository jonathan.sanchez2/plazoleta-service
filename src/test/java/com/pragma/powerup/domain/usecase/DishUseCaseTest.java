package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IRestaurantServicePort;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.spi.IDishPersistencePort;
import com.pragma.powerup.factory.FactoryDishDataTest;
import com.pragma.powerup.factory.FactoryRestaurantDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DishUseCaseTest {

    @InjectMocks
    DishUseCase dishUseCase;
    @Mock
    IDishPersistencePort dishPersistencePort;
    @Mock
    IRestaurantServicePort restaurantServicePort;

    @Test
    void saveDish() {
        //Given
        DishModel dishModel = FactoryDishDataTest.getDishModelWithIdNull();
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModel();
        //When
        when(restaurantServicePort.getRestaurantById(anyLong())).thenReturn(restaurantModel);
        dishUseCase.saveDish(dishModel);

        //Then
        verify(dishPersistencePort).saveDish(any(DishModel.class));
    }

    @Test
    void getDishById() {
        //Given
        Long idDish = FactoryDishDataTest.getExistingIdDish();
        Optional<DishModel> dishModel = FactoryDishDataTest.getOptionalDishModel();

        //When
        when(dishPersistencePort.getDishById(idDish)).thenReturn(dishModel);

        //Then
        assertEquals("Papa rellena",dishUseCase.getDishById(idDish).getName());
    }

    @Test
    void updateDish() {
        //Given
        Long idDish = FactoryDishDataTest.getExistingIdDish();
        DishModel dishModel = FactoryDishDataTest.getDishModelOnlyWithPriceAndDescription();
        Optional<DishModel> dishModelOptional = FactoryDishDataTest.getOptionalDishModel();
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModel();

        //When
        when(dishPersistencePort.getDishById(idDish)).thenReturn(dishModelOptional);
        when(restaurantServicePort.getRestaurantById(anyLong())).thenReturn(restaurantModel);
        dishUseCase.updateDish(idDish, dishModel);
        //Then
        verify(dishPersistencePort).saveDish(any(DishModel.class));

    }
}