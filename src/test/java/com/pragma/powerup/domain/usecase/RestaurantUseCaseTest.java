package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.factory.FactoryRestaurantDataTest;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantUseCaseTest {

    @InjectMocks
    RestaurantUseCase restaurantUseCase;
    @Mock
    IRestaurantPersistencePort restaurantPersistencePort;

    @Test
    void mustSaveARestaurant() {
        //Given
        RestaurantModel restaurantModel = FactoryRestaurantDataTest.getRestaurantModelWithIdNull();

        //When
        restaurantUseCase.saveRestaurant(restaurantModel);

        //Then
        verify(restaurantPersistencePort).saveRestaurant(any(RestaurantModel.class));

    }

    @Test
    void getRestaurantById() {
        //Given
        Long idRestaurant = FactoryRestaurantDataTest.getExistingIdRestaurant();
        Optional<RestaurantModel> restaurantModel = FactoryRestaurantDataTest.getOptionalRestaurantModel();

        //When
        when(restaurantPersistencePort.getRestaurantById(idRestaurant)).thenReturn(restaurantModel);

        //Then
        assertEquals("Otto Grill",restaurantUseCase.getRestaurantById(idRestaurant).getName());

    }

    @Test
    void getRestaurantByIdWhenIdDoesNotExistMustThrowNoDataFoundException() {
        //Given
        Long idRestaurant = FactoryRestaurantDataTest.getNoExistingIdRestaurant();

        //When
        when(restaurantPersistencePort.getRestaurantById(idRestaurant)).thenReturn(Optional.empty());

        //Then
        assertThrows(NoDataFoundException.class, () -> restaurantUseCase.getRestaurantById(idRestaurant));
    }

    @Test
    void getAllRestaurants() {
        //Given
        List<RestaurantModel> restaurantModel = FactoryRestaurantDataTest.getListRestaurantModel();
        //When
        when(restaurantPersistencePort.getAllRestaurants()).thenReturn(restaurantModel);
        //Then
        assertEquals(1,restaurantUseCase.getAllRestaurants().size());
    }
}