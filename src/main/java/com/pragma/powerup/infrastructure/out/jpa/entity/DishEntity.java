package com.pragma.powerup.infrastructure.out.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "dish")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@NonNull
@DynamicInsert
@DynamicUpdate
public class DishEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private double price;
    @Column(nullable = false)
    private String urlImage;
    @Column(nullable = false)
    private boolean activo=true;
    @ManyToOne
    @JoinColumn(name="category_id",nullable = false)
    private CategoryEntity category;
    @ManyToOne
    @JoinColumn(name="restaurant_id",nullable = false)
    private RestaurantEntity restaurant;
}
