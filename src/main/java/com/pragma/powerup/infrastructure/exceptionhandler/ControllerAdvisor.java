package com.pragma.powerup.infrastructure.exceptionhandler;

import com.pragma.powerup.domain.exception.ParameterIsNotPositiveNumericException;
import com.pragma.powerup.domain.exception.ParameterLengthException;
import com.pragma.powerup.domain.exception.ParameterCanNotBeOnlyNumericException;
import com.pragma.powerup.domain.exception.ParameterTypeException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    private static final String MESSAGE = "message";

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException ignoredNoDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_DATA_FOUND.getMessage()));
    }

    @ExceptionHandler(ParameterLengthException.class)
    public ResponseEntity<Map<String, String>> handleParameterLengthException(
            ParameterLengthException ignoredParameterLengthException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PARAMETER_LENGTH.getMessage()));
    }
    @ExceptionHandler(ParameterTypeException.class)
    public ResponseEntity<Map<String, String>> handleParameterTypeException(
            ParameterTypeException ignoredParameterTypeException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PARAMETER_TYPE.getMessage()));
    }
    @ExceptionHandler(ParameterCanNotBeOnlyNumericException.class)
    public ResponseEntity<Map<String, String>> handleParameterCanNotBeOnlyNumericException(
            ParameterCanNotBeOnlyNumericException ignoredParameterCanNotBeOnlyNumericException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PARAMETER_CAN_NOT_BE_ONLY_NUMERIC.getMessage()));
    }
    @ExceptionHandler(ParameterIsNotPositiveNumericException.class)
    public ResponseEntity<Map<String, String>> handleParameterIsNotPositiveNumericException(
            ParameterIsNotPositiveNumericException ignoredParameterIsNotPositiveNumericException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PARAMETER_IS_NOT_POSITIVE_NUMERIC.getMessage()));
    }
    
}