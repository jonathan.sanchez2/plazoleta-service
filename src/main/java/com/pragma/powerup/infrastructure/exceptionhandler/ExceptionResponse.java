package com.pragma.powerup.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    NO_DATA_FOUND("No data found for the requested petition"),
    PARAMETER_LENGTH("El valor ingresado no puede tener mas de 13 caracteres"),
    PARAMETER_TYPE("El valor ingresado no es numerico"),
    PARAMETER_CAN_NOT_BE_ONLY_NUMERIC("El valor ingresado no puede ser solo numerico"),
    PARAMETER_IS_NOT_POSITIVE_NUMERIC("El valor ingresado no es un numerico positivo");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}