package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NonNull
public class CategoryRequestDto {
    private String name;
    private String description;
}
