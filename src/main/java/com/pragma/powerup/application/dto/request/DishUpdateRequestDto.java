package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NonNull
public class DishUpdateRequestDto {
    private double price;
    private String description;
}
