package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DishRequestDto {
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private double price;
    @NotNull
    private String urlImage;
    private boolean activo;
    @NotNull
    private Long idCategory;
    @NotNull
    private Long idRestaurant;
}
