package com.pragma.powerup.application.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishResponseDto {

    private String name;
    private String description;
    private double price;
    private String urlImage;
    private boolean activo;
    private Long idCategory;
    private Long idRestaurant;
}
