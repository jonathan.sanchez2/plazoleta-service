package com.pragma.powerup.domain.spi;

import com.pragma.powerup.domain.model.RestaurantModel;
import java.util.List;
import java.util.Optional;

public interface IRestaurantPersistencePort {
    RestaurantModel saveRestaurant(RestaurantModel restaurantModel);
    Optional<RestaurantModel> getRestaurantById(Long id);
    List<RestaurantModel> getAllRestaurants();

}