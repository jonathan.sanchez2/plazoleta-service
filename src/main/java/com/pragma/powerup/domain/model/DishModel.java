package com.pragma.powerup.domain.model;

import com.pragma.powerup.domain.exception.ParameterIsNotPositiveNumericException;

public class DishModel {
    private Long id;
    private String name;
    private String description;
    private double price;
    private String urlImage;
    private boolean activo;
    private CategoryModel category;
    private RestaurantModel restaurant;

    public DishModel(Long id, String name, String description, double price, String urlImage, boolean activo, CategoryModel category, RestaurantModel restaurant) {

        if(price<=0){
            throw new ParameterIsNotPositiveNumericException();
        }

        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.urlImage = urlImage;
        this.activo = activo;
        this.category = category;
        this.restaurant = restaurant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {

        if(price<=0){
            throw new ParameterIsNotPositiveNumericException();
        }
        this.price = price;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public RestaurantModel getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantModel restaurant) {
        this.restaurant = restaurant;
    }
}
