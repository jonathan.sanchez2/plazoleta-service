package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.DishModel;

public interface IDishServicePort {

    void saveDish(DishModel dishModel);

    DishModel getDishById(Long id);

    DishModel updateDish(Long id, DishModel dishModel);
}