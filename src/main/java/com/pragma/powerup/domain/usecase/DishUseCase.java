package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IDishServicePort;
import com.pragma.powerup.domain.api.IRestaurantServicePort;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.spi.IDishPersistencePort;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;

public class DishUseCase implements IDishServicePort {

    private final IDishPersistencePort dishPersistencePort;
    private final IRestaurantServicePort restaurantServicePort;

    public DishUseCase(IDishPersistencePort dishPersistencePort, IRestaurantServicePort restaurantServicePort) {
        this.dishPersistencePort = dishPersistencePort;
        this.restaurantServicePort = restaurantServicePort;
    }
    @Override
    public void saveDish(DishModel dishModel) {

        RestaurantModel restaurant = restaurantServicePort.getRestaurantById(dishModel.getRestaurant().getId());
        dishModel.setRestaurant(restaurant);
        dishPersistencePort.saveDish(dishModel);
    }
    @Override
    public DishModel getDishById(Long id){
        return dishPersistencePort.getDishById(id).orElseThrow(NoDataFoundException::new);
    }
    @Override
    public DishModel updateDish(Long id, DishModel dishModel) {

        DishModel dish = getDishById(id);
        dish.setPrice(dishModel.getPrice());
        dish.setDescription(dishModel.getDescription());
        saveDish(dish);
        return dish;
    }


}