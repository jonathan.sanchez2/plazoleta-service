package com.pragma.powerup.domain.exception;

public class ParameterCanNotBeOnlyNumericException extends RuntimeException {
    public ParameterCanNotBeOnlyNumericException() {
        super();
    }
}
