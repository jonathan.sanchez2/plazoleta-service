package com.pragma.powerup.domain.exception;

public class ParameterLengthException extends RuntimeException {
    public ParameterLengthException() {
        super();
    }
}
