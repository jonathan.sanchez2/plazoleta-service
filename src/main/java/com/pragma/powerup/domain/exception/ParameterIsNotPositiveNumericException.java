package com.pragma.powerup.domain.exception;

public class ParameterIsNotPositiveNumericException extends RuntimeException {
    public ParameterIsNotPositiveNumericException() {
        super();
    }
}
